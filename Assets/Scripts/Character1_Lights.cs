﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hex #s 0-9, a-f.

public class Character1_Lights : MonoBehaviour 
{
	private static Character1_Lights _instance;

	public string CharacterValue = "0";

	public static Character1_Lights instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<Character1_Lights>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}
	
	public void UpdateCharacter(string _char) {
		//Debug.Log(_char);
		//LightManager.instance.ChangeColor(); 
		//Debug.Log("Character updated");
		if(_char == "0") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "1") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "2") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "3") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "4") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "5") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "6") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "7") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "8") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "9") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "a") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "b") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "c") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "d") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "e") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}

		if(_char == "f") { 
			Debug.Log(_char);
			LightManager.instance.UpdateLights(_char);
		}
		
	}

}
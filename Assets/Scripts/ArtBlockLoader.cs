﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ArtBlockLoader : MonoBehaviour {

 	public Text TextField;//TextField in Unity to display latest BLockHash
	public string CurrentBlockHash;
	private int TimerValue = 16;
	private char[] Characters;

    void Start() {
        StartCoroutine(RefreshData());
    }

	void Update() {
	    /*if (Input.GetKeyDown("f")) {
	         StartCoroutine(GetData());
	    }*/
	}

	//Every 15 or so seconds call GetData()
    IEnumerator RefreshData() {
		StartCoroutine(GetData());
		yield return new WaitForSeconds(TimerValue);
		StartCoroutine(RefreshData());
    }

	//Loads latest BlockHash from ArtBlocks endpoint.
	//value can be accesed as both a string and binary data
    IEnumerator GetData() {
        UnityWebRequest www = UnityWebRequest.Get("http://art-blocks-endpoints.whatsthescore.webfactional.com/homestead/0xb2f2337f812cdd216fb841d3453bbdd1647a1252");
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
			CurrentBlockHash = www.downloadHandler.text;
 			TextField.text = CurrentBlockHash;
            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
			ParseBlockHash();
        }
    }

	//Split BlockHash Characters into an Array
	void ParseBlockHash() { 
		Characters = CurrentBlockHash.ToCharArray();

		 // prints on the screen all the codes
		 /*foreach(char Current in Characters){
		    Debug.Log(Current);
		 }*/

		Debug.Log(Characters[2]);//first character
		Debug.Log(Characters[65]);//last character

		UpdateCharacterClasses();
	}

	void UpdateCharacterClasses() { 
		//Character1_Lights.instance.CharacterValue = Characters[2].ToString();
		Character1_Lights.instance.UpdateCharacter(Characters[2].ToString());
	}


}

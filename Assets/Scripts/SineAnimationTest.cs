﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineAnimationTest : MonoBehaviour
{

	private Vector3 _startPosition;
	private float SineRange = 6.0f;
	private float SineSpeed = 3.0f;

	void Start () 
	{
		_startPosition = transform.position;
	}
 
	void Update()
	{
		transform.position = _startPosition + new Vector3(Mathf.Sin(Time.time * SineSpeed) * SineRange, 0.0f, 0.0f);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour 
{
	private static LightManager _instance;

	public Light DirectionalLight1;
	public Light Light1;
	public Light Light2;

	public static LightManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<LightManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	public void ChangeColor() { 
		iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
		iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
		iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
	}

	public void UpdateLights(string _char) { 
		if(_char == "0") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
		}

		if(_char == "1") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
		}

		if(_char == "2") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
		}

		if(_char == "3") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
		}

		if(_char == "4") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
		}

		if(_char == "5") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
		}

		if(_char == "6") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
		}

		if(_char == "7") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
		}

		if(_char == "8") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.red, "time", 1f, "easetype", "linear"));
		}

		if(_char == "9") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
		}

		if(_char == "a") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
		}

		if(_char == "b") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.white, "time", 1f, "easetype", "linear"));
		}

		if(_char == "c") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.yellow, "time", 1f, "easetype", "linear"));
		}

		if(_char == "d") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.blue, "time", 1f, "easetype", "linear"));
		}

		if(_char == "e") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.green, "time", 1f, "easetype", "linear"));
		}

		if(_char == "f") { 
			iTween.ColorTo(Light1.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(Light2.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
			iTween.ColorTo(DirectionalLight1.gameObject, iTween.Hash("color", Color.magenta, "time", 1f, "easetype", "linear"));
		}
	}

	//iTween.ValueTo(gameObject,iTween.Hash("from",score,"to",score+bonus,"time",.6,"onUpdate","UpdateScoreDisplay"));

}

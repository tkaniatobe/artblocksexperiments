﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Hex #s 0-9, a-f.

public class Character2 : MonoBehaviour 
{
	private static Character2 _instance;

	public string CharacterValue = "0";

	public static Character2 instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<Character2>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}
	
	public void UpdateCharacter() {
		//LightManager.instance.ChangeColor(); 
		Debug.Log("Character updated");
		if(CharacterValue == "0") { 
			Debug.Log(0);
		}

		if(CharacterValue == "1") { 
			Debug.Log(1);
		}

		if(CharacterValue == "2") { 
			Debug.Log(2);
		}

		if(CharacterValue == "3") { 
			Debug.Log(3);
		}

		if(CharacterValue == "4") { 
			Debug.Log(4);
		}

		if(CharacterValue == "5") { 
			Debug.Log(5);
		}

		if(CharacterValue == "6") { 
			Debug.Log(6);
		}

		if(CharacterValue == "7") { 
			Debug.Log(7);
		}

		if(CharacterValue == "8") { 
			Debug.Log(8);
		}

		if(CharacterValue == "9") { 
			Debug.Log(9);
		}

		if(CharacterValue == "a") { 
			Debug.Log("a");
		}

		if(CharacterValue == "b") { 
			Debug.Log("b");
		}

		if(CharacterValue == "c") { 
			Debug.Log("c");
		}

		if(CharacterValue == "d") { 
			Debug.Log("d");
		}

		if(CharacterValue == "e") { 
			Debug.Log("e");
		}

		if(CharacterValue == "f") { 
			Debug.Log("f");
		}
		
	}

}